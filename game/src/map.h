/*
 * File:    map.h
 * Author:  Jan Remes (xremes00@stud.fit.vutbr.cz)
 * Project: TWO-KAI
 *
 * Description: Header for Map class
 */
#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#define MAP_SIZE 4

typedef enum {
    _UNDEFINED,
    UP,
    RIGHT,
    DOWN,
    LEFT
} operator_t;

class Map {
  public:
    unsigned int field[MAP_SIZE][MAP_SIZE];
    unsigned int old[MAP_SIZE][MAP_SIZE];
    bool valid;


    /**
     * Create empty map
     */
    Map ();

    /**
     * Move the tiles in specified direction
     */
    bool move(operator_t direction);

    /**
     * Add new tile
     */
    bool add_tile(void);

    /**
     * Print map
     */
    void print(void);

    void fprint(void);

    /**
     * Check for playable - if not playable, player lost
     */
    bool playable(void);



  private:
    Map& rotate_left(void);
    Map& rotate_right(void);
    Map& rotate_180(void);

    Map& move_left(void);

    void save(void);
};


#endif // MAP_H_INCLUDED
