#include "map.h"

#include <iostream>
#include <string>
#include <time.h>

void game(void)
{
    Map* map = new Map;
    std::string line;
    operator_t op;

    map->add_tile();
    map->print();

    while(true)
    {
        std::getline(std::cin, line);

        if(line == "U" || line == "UP")
            op = UP;
        else if(line == "R" || line == "RIGHT")
            op = RIGHT;
        else if(line == "D" || line == "DOWN")
            op = DOWN;
        else if(line == "L" || line == "LEFT")
            op = LEFT;
        else op = _UNDEFINED;

        if(op == _UNDEFINED)
        {
            std::cout << "INVALID COMMAND" << std::endl;
            continue;
        }

        if(!map->move(op))
        {
            std::cout << "INVALID OPERATION" << std::endl;
            map->print();
            continue;
        }
        else
        {
            std::cout << "OK" << std::endl;
            map->add_tile();    // cannot fail, as the move() must have been succesful
            map->print();
            if(!map->playable())
            {
                std::cout << "BYE" << std::endl;
                return;
            }
        }
    }
}

int main(void)
{
    srand(clock());
    std::cout << "# Starting THE GAME" << std::endl;

    game();

}
