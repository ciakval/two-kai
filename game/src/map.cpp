/*
 * File:    map.cpp
 * Author:  Jan Remeš (xremes00@stud.fit.vutbr.cz)
 * Project: TWO-KAI
 *
 * Description: Contains Map class
 */

#include "map.h"

#include <cstring>
#include <cstdlib>
#include <cstdio>

Map::Map() {
    memset(this->field, 0, sizeof(this->field));
    this->valid = true;
}

bool Map::move(operator_t direction) {
    switch(direction)
    {
        case UP:
            this->rotate_left().move_left().rotate_right();
            break;
        case RIGHT:
            this->rotate_180().move_left().rotate_180();
            break;
        case DOWN:
            this->rotate_right().move_left().rotate_left();
            break;
        case LEFT:
            this->move_left();
            break;
        default:
            throw "Unknown Operator";
    }

    return this->valid;
}

bool Map::add_tile(void) {
    unsigned int number = (rand() & 0x00004000) ? 4 : 2;

    unsigned int zeros = 0;

    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            if(this->field[i][j] == 0)
            {
                zeros++;
            }
        }
    }

    if(zeros == 0)
        return false;

    unsigned int position = rand() % zeros;

    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            if(this->field[i][j] == 0)
            {
                if(!(position--))
                {
                    this->field[i][j] = number;
                    return true;
                }
            }
        }
    }

    return false;
}

void Map::print() {
    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            printf("%8d ", this->field[i][j]);
        }
        putchar('\n');
    }
}

void Map::fprint() {
    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            fprintf(stderr, "%8d ", this->field[i][j]);
        }
        fputc('\n', stderr);
    }
}

bool Map::playable(void)
{
    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            if((field[i][j] == 0))
            {
                return true;
            }

            if(i < MAP_SIZE - 1)
            {
                if(field[i][j] == field[i+1][j])
                    return true;
            }

            if(j < MAP_SIZE - 1)
            {
                if(field[i][j] == field[i][j+1])
                    return true;
            }
                    
        }
    }
    return false;
}
        

Map& Map::rotate_left(void) {
    save();
    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            this->field[i][j] = this->old[j][MAP_SIZE - i - 1];
        }
    }

    return *this;
}


Map& Map::rotate_right(void) {
    save();
    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            this->field[i][j] = this->old[MAP_SIZE - j - 1][i];
        }
    }

    return *this;
}


Map& Map::rotate_180(void) {
    save();
    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            this->field[i][j] = this->old[MAP_SIZE - i - 1][MAP_SIZE - j - 1];
        }
    }

    return *this;
}

Map& Map::move_left(void) {
    bool merged = false;
    bool moved = false;

    // Work independently for each row
    for(int index = 0; index < MAP_SIZE; index++)
    {
        // MERGING
        for(int i = 0; i < MAP_SIZE; i++)
        {
            // do not merge zeros
            if(this->field[index][i] == 0)
                continue;

            // find closest matching, skip zeros, break on other
            for(int j = i + 1; j < MAP_SIZE; j++)
            {
                if(this->field[index][i] == this->field[index][j])
                {
                    this->field[index][i] *= 2;
                    this->field[index][j] = 0;
                    merged = true;
                    break;
                }
                else if(this->field[index][j] == 0)
                    continue;

                else break;
            }
        }

        // MOVING
        int offset = 0;

        for(int i = 0; i < MAP_SIZE; i++)
        {
            unsigned int movnumber = this->field[index][i];
            this->field[index][i] = 0;
            this->field[index][i-offset] = movnumber;

            if(movnumber == 0)
                offset++;

            if(movnumber && offset)
                moved = true;
        }
    }

    this->valid = merged || moved;

    return *this;
}

void Map::save(void) {
    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            old[i][j] = field[i][j];
        }
    }
}
