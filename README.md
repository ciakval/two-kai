# Two-KAI #
This project aims to create text-based version of popular game '2048', thus 
providing the base to create AI players of the game.

### Writing an AI player ###
Write your player either as a binary accepting Two-KAI's binary as an argument and processing its input and output internally, or as a filter and use named pipes to connect both programs. Consult [Wiki](https://bitbucket.org/ciakval/two-kai/wiki) for syntax.

### Contributors ###
* Developer: Jan Remes (xremes00@stud.fit.vutbr.cz)