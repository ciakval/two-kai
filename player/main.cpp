/*
 * File:    player/main.cpp
 * Author:  Jan Remeš (xremes00@stud.fit.vutbr.cz)
 * Project: two-kai
 *
 * Description: This file contains core functions for AI player
 */


/********************************************************************* *
 * ~~~~[ INCLUDES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * ********************************************************************/
#include "../game/src/map.h"

#include <iostream>
#include <string>

using namespace std;

/********************************************************************* *
 * ~~~~[ PROTOTYPES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * ********************************************************************/
 
/**
 * External function play() for allowing various algorithms
 */
operator_t play(Map& current);

/********************************************************************* *
 * ~~~~[ FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * ********************************************************************/
 
/**
 * Read map field from string
 *
 * Return Map object on success, or NULL on error
 */
Map * readMap(string received)
{
	Map * result = new Map;

    if(sscanf(received.c_str(), "%u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u", 
                &(result->field[0][0]), &(result->field[0][1]), &(result->field[0][2]), &(result->field[0][3]),
                &(result->field[1][0]), &(result->field[1][1]), &(result->field[1][2]), &(result->field[1][3]),
                &(result->field[2][0]), &(result->field[2][1]), &(result->field[2][2]), &(result->field[2][3]),
                &(result->field[3][0]), &(result->field[3][1]), &(result->field[3][2]), &(result->field[3][3]))
            != 16)
    {
        delete result;
        std::cerr << "Reading map failed" << std::endl;
        return NULL;
    }

    result->valid = true;

    return result;
}

/**
 * Report reached values exceeding threshold
 */
void report (Map& map, int threshold = 512)
{
    for(int i = 0; i < MAP_SIZE; i++)
    {
        for(int j = 0; j < MAP_SIZE; j++)
        {
            if(map.field[i][j] >= threshold)
            {
                std::cerr << "Reached " << map.field[i][j] << std::endl;
            }
        }
    }
}

/********************************************************************* *
 * ~~~~[ MAIN function ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * ********************************************************************/
int main(void)
{
    string line, received;
    Map *map;
    operator_t op;

    do {
        received = string();

        for(int i = 0; i < MAP_SIZE; i++)
        {
            getline(cin, line);
            if(line[0] == '#')
            {
                i--;
                continue;
            }
            received += line;
        }

        map = readMap(received);

        if(map->playable())
        {
            op = play(*map);

            switch(op)
            {
                case LEFT:
                    cout << "LEFT" << endl;
                    break;
                case UP:
                    cout << "UP" << endl;
                    break;
                case RIGHT:
                    cout << "RIGHT" << endl;
                    break;
                case DOWN:
                    cout << "DOWN" << endl;
                    break;
                default:
                    cerr << "Unknown operator" << endl;
                    return -1;
            }
        }

        do {
            getline(cin,line);
        } while (line[0] == '#');

        if(line != "OK")
        {
            if(line == "BYE")
            {
                cerr << "Game said BYE" << endl;
                map->fprint();
                delete map;
                break;
            }
            else if(line == "INVALID OPERATION")
            {
                cerr << "Algorithm executed invalid operation" << endl;
            }
            else
            {
                cerr << "PANIC! Received status '" << line << "'!" << endl;
                delete map;
                return -2;
            }
        }

        delete map;

    } while(1);

    return 0;
}
