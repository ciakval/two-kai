/*
 * File:    player/algorithm/random.c
 * Author:  Jan Remeš (xremes00@stud.fit.vutbr.cz)
 * Project: two-kai
 */

#include "../../game/src/map.h"

#include <cstdlib>
#include <ctime>

static int seed = 0;

operator_t play(Map& current)
{
    if(!seed)
    {
        seed = time(NULL);
        srand(seed);
    }

    switch(rand() % 4)
    {
        case 0:
            return LEFT;
        case 1:
            return UP;
        case 2:
            return RIGHT;
        case 3:
            return DOWN;
        default:
            return _UNDEFINED;
    }
}

