/*
 * File:    player/algorithm/prefer.c
 * Author:  Jan Remeš (xremes00@stud.fit.vutbr.cz)
 * Project: two-kai
 */

#include "../../game/src/map.h"

#define TRY(x) if(map.move(x)) { return(x); }

operator_t play (Map& map)
{
    TRY(LEFT)
        else TRY(UP)
            else TRY(DOWN)
                else TRY(RIGHT)

    return _UNDEFINED;
    

}
